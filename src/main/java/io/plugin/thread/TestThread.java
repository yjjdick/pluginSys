package io.plugin.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestThread extends Thread {
    public static void main(String[] args) {
        TestThread testThread = new TestThread();
        log.info("线程启动>>>>>>>>>>>>>>");
        testThread.start();

        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("线程interrupt>>>>>>>>>>>>>>");
        testThread.interrupt();

    }

    public void run() {
        boolean flag = true;
        while (!this.isInterrupted() && flag) {
            try {
                log.info("Thread is running >>>>>>>>>>");
                long time = System.currentTimeMillis();
                sleep(2000);
            } catch (InterruptedException e) {
                flag = false;
            }
        }
    }
}
