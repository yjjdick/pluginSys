package io.plugin.httpclient;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.httpclient")
@Data
public class HttpClientProperties {

    private Integer connectTimeOut = 30000;
    private Integer socketTimeOut = 30000;

    private String agent = "agent";
    private Integer maxConnPerRoute = 10;
    private Integer maxConnTotool = 50;

}
