package io.plugin.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    SYSTEM_SUCCESS(0, "成功"),//成功
    SYSTM_ERROR(500, "系统错误"),//系统错误
    PARAM_ERROR(1, "参数不正确"),//参数不正确

    GAME_LOGIN_FAILED(2, "登录失败"),//登录失败
    GAME_GET_NEWSTAGE_FAILED(3, "获取新期数失败"),//获取新期数失败
    GAME_PLAY_FAILED(4, "游戏下注失败"),//游戏下注失败
    GAME_GET_USER_BALANCE_FAILED(5, "获取用户余额失败"),//获取用户余额失败
    GAME_RGPLAN_FAILED(6, "计划计算有错"),//计划计算有错
    ;

    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
