package io.plugin.enums;

import lombok.Getter;

@Getter
public enum PlanEchartDateEnum implements IEnum {
    ONE_WEEK(1,"一周"),
    TOW_WEEK(2,"两周"),
    THREE_WEEK(3,"三周"),
    MONTH(4,"一个月")
    ;

    private Integer code;
    private String message;
    PlanEchartDateEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
