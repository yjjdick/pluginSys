package io.plugin.enums;

public interface IEnum {
    Integer getCode();
    String getMessage();
}
