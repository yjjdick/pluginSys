package io.plugin.enums;

import lombok.Getter;

@Getter
public enum PlanTypeEnum {

    ZT(0,"总统"),
    NW(1,"女娲"),
    ST(2,"通神"),
    ST1(3,"通神1"),
    ST2(4,"通神2"),
    SG(5,"时光"),
    FH(6,"凤凰"),
    FG(7,"富贵")
    ;

    private Integer code;
    private String message;
    PlanTypeEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
