package io.plugin.service;

import io.plugin.model.PlanLog;


/**
 * 系统用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:43:39
 */
public interface SSCLogService extends BaseService<PlanLog> {
	boolean updatePlan(Integer type);
}
