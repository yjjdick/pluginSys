package io.plugin.service;

import io.plugin.common.utils.PageUtils;
import io.plugin.entity.EchartPlan;
import io.plugin.enums.PlanEchartDateEnum;
import io.plugin.model.PlanLog;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:43:39
 */
public interface SysPlanLogService extends BaseService<PlanLog> {

	boolean updatePlan(Integer type);

	PageUtils queryPage(Map<String, Object> params);

	List<EchartPlan> echart(PlanEchartDateEnum planEchartDateEnum);
}
