package io.plugin.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import io.plugin.common.entity.Filter;
import io.plugin.common.entity.Order;

import java.util.List;

public interface BaseService<M extends Model<M>> {

    List<M> findAll();

    M findById(Object ...id);

    List<M> findByModel(M model);

    M findFirstByModel(M model);

    List<M> find(SqlPara sqlPara);

    M findFirst(SqlPara sqlPara);

    boolean deleteBatch(Object[]... ids);

    boolean deleteBatch(String[] columns, Object[]... ids);

    boolean deleteByModel(M model);

    Page<M> paginate(Integer pageNum, Integer pageSize, List<Filter> filters, Order order);

    Page<M> paginate(Integer pageNum, Integer pageSize, List<Filter> filters);

    Page<M> paginate(Integer pageNum, Integer pageSize);

    Page<M> paginate(Integer pageNum, Integer pageSize, List<Filter> filters, List<Order> orders);

    Page<M> paginate(Integer pageNum, Integer pageSize, Filter filter, List<Order> orders);

    Page<M> paginate(Integer pageNum, Integer pageSize, Filter filter, Order order);

    M insert(M model);

    List<M> findByFilter(Filter filter);

    List<M> findByFilters(List<Filter> filter);

    boolean update(M model);

    boolean delete(M model);
}
