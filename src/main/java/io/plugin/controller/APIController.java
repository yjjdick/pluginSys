package io.plugin.controller;

import io.plugin.entity.SSCResultItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@Slf4j
@RequestMapping("/api")
public class APIController {

    @Autowired
    RestTemplate directRestTemplate;


    @Autowired
    RestTemplate normalRestTemplate;

    @RequestMapping("/test1")
    public ModelAndView test1(ModelAndView modelAndView) {
        modelAndView.setViewName("sscResult");
        ResponseEntity<String> response = directRestTemplate.getForEntity("https://cqssc.17500.cn/data/cqssc.txt", String.class);
        String body = response.getBody();
        String[] itemArr = body.split("\r\n");
        List<SSCResultItem> sscResultItemList = new ArrayList<SSCResultItem>();

        for (String item:itemArr
             ) {
            String no = item.substring(0, 11);
            String code = item.substring(11).replace(" ","");
            SSCResultItem sscResultItem = new SSCResultItem();
            sscResultItem.setCode(code);
            sscResultItem.setNo(no);
            sscResultItemList.add(sscResultItem);
        }

        Collections.reverse(sscResultItemList);

        modelAndView.addObject("sscResultItemList", sscResultItemList);
        return modelAndView;

    }

    @RequestMapping("/test")
    public ModelAndView test(ModelAndView modelAndView) {
        modelAndView.setViewName("sscResult");
        ResponseEntity<String> response = directRestTemplate.getForEntity("https://cqssc.17500.cn/data/cqssc.txt", String.class);
        String body = response.getBody();
        String[] itemArr = body.split("\r\n");
        List<SSCResultItem> sscResultItemList = new ArrayList<SSCResultItem>();

        for (String item:itemArr
             ) {
            String no = item.substring(0, 11);
            String code = item.substring(11).replace(" ","");
            SSCResultItem sscResultItem = new SSCResultItem();
            sscResultItem.setCode(code);
            sscResultItem.setNo(no);
            sscResultItemList.add(sscResultItem);
        }

        Collections.reverse(sscResultItemList);

        modelAndView.addObject("sscResultItemList", sscResultItemList);
        return modelAndView;

    }

    public static void main(String[] args) {
//        String code = getRGPlan("57027" ,"AA9D868A042C3B93/69");
//        String code = getRGPlan("57027" ,"85D23ACE-30");
//        System.out.println("code>>>>>>>>>>>>>>>>>>" + code);

//        getAllStageInfo
    }


}
