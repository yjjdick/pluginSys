package io.plugin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller
@RequestMapping("/test")
@Slf4j
public class TestController {


    @GetMapping("/hello")
    public ModelAndView hello(ModelAndView modelAndView, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=test.doc");
        response.setContentType("application/octet-stream;charset=UTF-8");
        modelAndView.addObject("value", "test");
        modelAndView.setViewName("hello");
        return modelAndView;
    }

    @GetMapping("/ip")
    @ResponseBody
    public String print() {
        String host = null;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.error("get server host Exception e:", e);
        }
        return host;
    }

}