/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package io.plugin.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.plugin.common.utils.EnumUtil;
import io.plugin.common.utils.PageUtils;
import io.plugin.common.utils.R;
import io.plugin.entity.EchartPlan;
import io.plugin.enums.PlanEchartDateEnum;
import io.plugin.enums.PlanTypeEnum;
import io.plugin.service.SysPlanLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 系统日志
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-03-08 10:40:56
 */
@Controller
@RequestMapping("/sys/lottery")
public class SysLotteryController {

	@Autowired
	private SysPlanLogService sysPlanLogService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/planLog/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysPlanLogService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/planLog/echart")
	public R echart(@RequestParam Map<String, Object> params){
		Integer type = Integer.parseInt(params.getOrDefault("type", PlanEchartDateEnum.ONE_WEEK.getCode()).toString());
		List<String> xAxisList = new ArrayList<>();
		for (PlanTypeEnum each: PlanTypeEnum.values()) {
			xAxisList.add(each.getMessage());
		}

		PlanEchartDateEnum planEchartDateEnum = EnumUtil.getByCode(type, PlanEchartDateEnum.class);
		int offsetDay = 0;
		switch (planEchartDateEnum) {
			case ONE_WEEK:
				offsetDay = 7;
				break;
			case TOW_WEEK:
				offsetDay = 14;
				break;
			case THREE_WEEK:
				offsetDay = 21;
				break;
			case MONTH:
				offsetDay = 30;
				break;
		}

		List<String> yAxisList = new ArrayList<>();
		Date beginOfDay = DateUtil.beginOfDay(new Date());
		for(int i = 1;i <= offsetDay; i ++) {
			DateTime beginDateTime = DateUtil.offsetDay(beginOfDay, -i);
			yAxisList.add(beginDateTime.toString(DatePattern.NORM_DATE_PATTERN));
		}

		List<EchartPlan> echartPlanList = sysPlanLogService.echart(planEchartDateEnum);
		return R.ok()
				.put("echartPlanList", echartPlanList)
				.put("xAxisList", yAxisList)
				.put("yAxisList", xAxisList);
	}
	
}
