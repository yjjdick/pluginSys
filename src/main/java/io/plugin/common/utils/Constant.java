package io.plugin.common.utils;

/**
 * 常量
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月15日 下午1:23:52
 */
public class Constant {
    /**
     * 超级管理员ID
     */
    public static final int SUPER_ADMIN = 1;

	public static final String SESSID_KEY = "sessid";
	public static final String GMAE_LOG_KEY = "game:log:";
	public static final String GMAE_MODE = "game:mode";
	public static final String GMAE_PLAN_TYPE = "game:plantype";
	public static final String GMAE_PROFIT = "game:profit";
	public static final String GMAE_TAR_BALANCE = "game:tarbalance";
	public static final String GMAE_INIT_BALANCE = "game:initbalance";
	public static final String GMAE_USER_BALANCE = "game:userbalance";
    public static final String GMAE_RATE = "game:rate";
    public static final String GMAE_CUR_FOLLOW_COUNT = "game:cur_follow_count";
    public static final String GMAE_START = "game:start";
    public static final String GMAE_PRE_PLAY_CODE = "game:pre_play_code";
    public static final String GMAE_LAST_PLAY_STAGE = "game:last_play_stage";
    public static final String GMAE_LAST_PLAN_CODE = "game:last_plan_code";
    public static final String GMAE_LAST_PLAN_STAGE = "game:last_plan_stage";
    public static final String GMAE_LAST_PLAN_COUNT = "game:last_plan_count";
    public static final String GMAE_MAX_PLAY_RATE_COUNT = "game:max_play_rate_count";
    public static final String GMAE_ERROR_FOLLOW_MONEY = "game:error_follow_money";
    public static final String GMAE_PROXY = "game:proxy";
    public static final String GMAE_PROXY_EXPIRE = "game:proxy_expire";
    public static final String GMAE_LINE = "game:line";

	/**
	 * 菜单类型
	 * 
	 * @author chenshun
	 * @email sunlightcs@gmail.com
	 * @date 2016年11月15日 下午1:24:29
	 */
    public enum MenuType {
        /**
         * 目录
         */
    	CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * 定时任务状态
     * 
     * @author chenshun
     * @email sunlightcs@gmail.com
     * @date 2016年12月3日 上午12:07:22
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
    	NORMAL(0),
        /**
         * 暂停
         */
    	PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 是否上架
     */
    public enum Marketable {
        /**
         * 下架
         */
        OBTAINED(0),
        /**
         * 上架
         */
        SHELF(1);

        private int value;

        Marketable(int value) {this.value = value; }

        public int getValue() {return value;}
    }

}
