package io.plugin.common.exception;

import io.plugin.enums.ResultEnum;

public class GameException extends RRException implements RetryException {
    public GameException(String msg) {
        super(msg);
    }

    public GameException(ResultEnum resultEnum) {
        super(resultEnum);
    }
}
