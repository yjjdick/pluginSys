package io.plugin.dao;

import io.plugin.model.SscLog;
import org.springframework.stereotype.Component;

@Component
public class SSCLogDao extends BaseDao<SscLog> {
    public SSCLogDao() {
        super(SscLog.class);
    }
}
