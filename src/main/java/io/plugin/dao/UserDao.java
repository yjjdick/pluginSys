package io.plugin.dao;

import io.plugin.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserDao extends BaseDao<User> {
    public UserDao() {
        super(User.class);
    }
}
