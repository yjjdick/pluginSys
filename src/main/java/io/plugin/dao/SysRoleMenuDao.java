package io.plugin.dao;

import io.plugin.model.SysRoleMenu;
import org.springframework.stereotype.Component;

@Component
public class SysRoleMenuDao extends BaseDao<SysRoleMenu> {
    public SysRoleMenuDao() {
        super(SysRoleMenu.class);
    }
}
