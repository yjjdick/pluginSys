package io.plugin.dao;

import io.plugin.model.SysLog;
import org.springframework.stereotype.Component;

@Component
public class SysLogDao extends BaseDao<SysLog> {
    public SysLogDao() {
        super(SysLog.class);
    }
}
