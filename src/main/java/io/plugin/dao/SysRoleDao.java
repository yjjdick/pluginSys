package io.plugin.dao;

import io.plugin.model.SysRole;
import org.springframework.stereotype.Component;

@Component
public class SysRoleDao extends BaseDao<SysRole> {
    public SysRoleDao() {
        super(SysRole.class);
    }
}
