package io.plugin.dao;

import io.plugin.model.SysConfig;
import org.springframework.stereotype.Component;

@Component
public class SysConfigDao extends BaseDao<SysConfig> {
    public SysConfigDao() {
        super(SysConfig.class);
    }
}
