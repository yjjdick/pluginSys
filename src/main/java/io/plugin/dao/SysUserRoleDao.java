package io.plugin.dao;

import io.plugin.model.SysUserRole;
import org.springframework.stereotype.Component;

@Component
public class SysUserRoleDao extends BaseDao<SysUserRole> {
    public SysUserRoleDao() {
        super(SysUserRole.class);
    }
}
