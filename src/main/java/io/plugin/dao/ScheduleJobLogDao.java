package io.plugin.dao;

import io.plugin.model.ScheduleJobLog;
import org.springframework.stereotype.Component;

@Component
public class ScheduleJobLogDao extends BaseDao<ScheduleJobLog> {
    public ScheduleJobLogDao() {
        super(ScheduleJobLog.class);
    }
}
