package io.plugin.dao;

import io.plugin.model.PlanLog;
import org.springframework.stereotype.Component;

@Component
public class PlanLogDao extends BaseDao<PlanLog> {
    public PlanLogDao() {
        super(PlanLog.class);
    }
}
