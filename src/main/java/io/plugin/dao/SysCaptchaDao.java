package io.plugin.dao;

import io.plugin.model.SysCaptcha;
import org.springframework.stereotype.Component;

@Component
public class SysCaptchaDao extends BaseDao<SysCaptcha> {
    public SysCaptchaDao() {
        super(SysCaptcha.class);
    }
}
