package io.plugin.entity;

import lombok.Data;

@Data
public class RGPlanItem {
    String issue;
    String rewardCode;
    String planCode;
    Integer status;
    String content;
}
