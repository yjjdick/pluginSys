package io.plugin.entity;

import lombok.Data;

@Data
public class ProxyItem {
    String ip;
    Integer port;
}
