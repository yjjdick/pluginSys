package io.plugin.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EchartPlan {
    String name;
    String type = "line";
    String stack = "总量";
    List<Object> data = new ArrayList<>();
}
