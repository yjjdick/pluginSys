package io.plugin.entity;

import lombok.Data;

@Data
public class SSCResultItem {
    String no;
    String code;
}
