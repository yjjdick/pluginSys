#namespace("common")
  #include("common.sql")
#end

#namespace("sysUser")
  #include("sysUser.sql")
#end

#namespace("sysMenu")
  #include("sysMenu.sql")
#end

#namespace("sysConfig")
  #include("sysConfig.sql")
#end

#namespace("news")
  #include("news.sql")
#end