package io.plugin.service.impl;

import io.plugin.common.utils.PageUtils;
import io.plugin.service.SysPlanLogService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class SysPlanLogServiceImplTest {
    @Autowired
    SysPlanLogService sysPlanLogService;

    @Test
    public void queryPage() {

        Map<String, Object> map = new HashMap<>();
        map.put("type", 1);
        map.put("planDate", "2018-09-22");

        PageUtils pageUtils = sysPlanLogService.queryPage(map);
    }
}