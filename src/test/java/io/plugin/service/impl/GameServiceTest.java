package io.plugin.service.impl;

import io.plugin.entity.RGPlanItem;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class GameServiceTest {

    @Autowired
    GameService gameService;

    @Test
    public void login() {
        Boolean b = gameService.tryLogin(false);

        log.info("b >>>>>>>>>>> {}", b);
    }

    @Test
    public void tryGetNetStage() {
        String netStage = gameService.tryGetNetStage();
        log.info("netStage >>>>>>>>>>> {}", netStage);
    }

    @Test
    public void tryGetLastStage() {
        Map<String, String> map = gameService.tryGetLastStageInfo();
        log.info("map >>>>>>>>>>> {}", map);
    }

    @Test
    public void getUserBalance() {
        Map<String, String> map = gameService.getLastStageInfoEx("20180928070");
        log.info("map >>>>>>>>>>>>>>>>>>>>>> {}", map);
    }

    @Test
    public void start() {
        Map<String, String> map = gameService.getLastStageInfoEx("20180928070");
        log.info("map >>>>>>>>>>>>>>>>>>>>>> {}", map);
    }


    @Test
    public void getLastStageInfoEx() {
        Map<String, String> map = gameService.getLastStageInfoEx("20180928061");
        log.info("map >>>>>>>>>>>>>>>>>>>>>> {}", map);
    }

    @Test
    public void getLastStageInfoEx2() {

        Map<String, String> map = null;
        do {
            map = gameService.getLastStageInfoEx2("20181011061");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (map == null);

        log.info("map >>>>>>>>>>>>>>>>>>>>>> {}", map);
    }

    @Test
    public void tryGetPlan() {
        Long start = System.currentTimeMillis();
        gameService.tryGetPlan(0);
        Long stop = System.currentTimeMillis();
        log.info("exe time >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}", stop - start);
    }

    @Test
    public void getAllStageInfo() {
        List<RGPlanItem> rgPlanItemList = gameService.getAllStageInfo(1,"9C911C1CB923B9B5*87", 3);
        BigDecimal profit = BigDecimal.ZERO;
        Integer errorCount = 0;
        for (RGPlanItem rgPlanItem :rgPlanItemList
             ) {
            if(rgPlanItem.getContent().equals("中1")) {
                profit = profit.add(BigDecimal.valueOf(0.9));
            }else if(rgPlanItem.getContent().equals("中2")) {
                profit = profit.add(BigDecimal.valueOf(1.8));
            }else if(rgPlanItem.getContent().equals("中3")) {
                profit = profit.add(BigDecimal.valueOf(4.5));
            }else if(rgPlanItem.getContent().equals("中4")) {
                profit = profit.add(BigDecimal.valueOf(12.65));
            }else if(rgPlanItem.getContent().equals("中5")) {
                profit = profit.add(BigDecimal.valueOf(36.95));
            }else if(rgPlanItem.getContent().equals("中6")) {
                profit = profit.add(BigDecimal.valueOf(100.98));
            }else if(rgPlanItem.getContent().equals("错")) {
                errorCount++;
                profit = profit.subtract(BigDecimal.valueOf(13));
            }
            log.info(String.format("issue = %s winnum = %s planCode = %s status = %s",rgPlanItem.getIssue(), rgPlanItem.getRewardCode(), rgPlanItem.getPlanCode(), rgPlanItem.getContent()));
        }



        log.info("profit >>>>>>>>>>>>>>>>>>>>>>>{}", profit.toString());
        log.info("正确率 >>>>>>>>>>>>>>>>>>>>>>>{}", ((rgPlanItemList.size() - errorCount) / (double)rgPlanItemList.size()) + "");
    }

    @Test
    public void loginT() {
        gameService.loginT();
    }
}