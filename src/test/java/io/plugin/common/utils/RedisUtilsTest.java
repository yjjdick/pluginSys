package io.plugin.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisUtilsTest {

    @Autowired
    RedisUtils redisUtils;

    @Test
    public void set() {
        redisUtils.delete("test:*");
    }

    @Test
    public void get() {
        String test = redisUtils.get("a321");
    }
}